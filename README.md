# info_cluster



## Запуск
```bash
python info_cluster.py -p output.yml
```

Параметры:
* "-p" required=True. 'FILE PATH IN'
* "--ph" required=False. HOSTS FILE OUTPUT PATH. Default = 'hosts.md'
* "-ps" required=False. SERVICES FILE OUTPUT PATH. Default = 'services.md'