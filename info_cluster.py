import os
import sys
import argparse
import pandas as pd
import yaml

PATH_IN = 'output.yml'
PATH_OUT = 'statistics.md'
CONFIG_PATH = 'config.yml'
PATH_CLUSTER_CONFIG = 'dev-ep.yml'
CONFIG = None
list_enabled_services = []

def serv_container_handling(resources_services_list, service_name, pods_main_services):
    global list_enabled_services
    ignore_cnhost_taint = pods_main_services['affinities']['ignore_cnhost_taint'] if 'ignore_cnhost_taint' in pods_main_services['affinities'] else False
    node_affinity_host_type = pods_main_services['affinities']['node_affinity_host_type']

    containers_services = pd.Series(pods_main_services['containers'])

    #replicas = 1 + (pods_main_services['replicas'] if 'replicas' in pods_main_services else(
    #    1 if service_name in CONFIG['exceptions_service'] else 0
    #))

    for j in range(containers_services.index.size):
        if 'resources' in containers_services[j]:
            container = pd.Series(containers_services[j]).dropna()
            resources_services_list.append({
                'service_name': service_name,
                'container': containers_services.index[j],
                'cpu': container['resources']['cpu'] if 'cpu' in container['resources'] else 0,
                'cpu_max': container['resources']['cpu_max'] if 'cpu_max' in container['resources'] else container['resources']['cpu'] if 'cpu' in container['resources'] else 0,
                'memory': container['resources']['memory'] if 'memory' in container['resources'] else 0,
                'memory_max': container['resources']['memory_max'] if 'memory_max' in container['resources'] else 0,
                'replicas': pods_main_services['replicas'] if 'replicas' in pods_main_services else 0,
                'ignore_cnhost_taint': ignore_cnhost_taint,
                'node_affinity_host_type': node_affinity_host_type,
                'in_exceptions_service': 1 if service_name in CONFIG['exceptions_service'] else 0,
                'enabled': 1 if service_name in list_enabled_services else 0
            })

def sum_secondary_disk(list_disk_secondary):
    sum = 0
    for i in range(list_disk_secondary.index.size):
        sum += list_disk_secondary[i]['size']
    return sum

def main():
    # Download data from YAML
    with open(PATH_IN, 'r') as file:
        data = yaml.safe_load(file)

    # Create table from data
    df = pd.DataFrame(data)

    # Generation of hosts data
    dhosts = pd.Series(df['hosts'].dropna())
    res_hosts_list = []

    for i in range(dhosts.size):
        child_hosts = pd.DataFrame(dhosts[i]['hosts'])
        for j in range(child_hosts.index.size):
            res_hosts_list.append({
                'host': dhosts.index[i],
                'ip_address': child_hosts.loc[j]['provider_specific']['host_system'],
                'cpu': child_hosts.loc[j]['resources']['cpu']['cores'] * CONFIG['cpu_freq'],
                'disks_primary': child_hosts.loc[j]['resources']['disks']['primary']['size'],
                'disks_secondary': sum_secondary_disk(pd.Series(child_hosts.loc[j]['resources']['disks']['secondary'])) if 'secondary' in child_hosts.loc[j]['resources']['disks'] else 0,
                'memory': child_hosts.loc[j]['resources']['memory'] * CONFIG['size_gigabyte'],
            })

    # Generation of services data
    dservices = pd.Series(df['services'].dropna())
    res_services_list = []
    for i in range(dservices.size):
        name = dservices.index[i]

        if 'pods' in dservices[i]:
            pods_main_services = pd.Series(dservices[i]['pods']['main'])
            serv_container_handling(res_services_list, dservices.index[i], pods_main_services)

        else:
            child_services = pd.Series(dservices[i])
            for j in range(child_services.index.size):
                if 'pods' in child_services[j]:
                    pods_main_services = pd.Series(child_services[j]['pods']['main'])
                    serv_container_handling(res_services_list, dservices.index[i], pods_main_services)

    res_hosts = pd.DataFrame(res_hosts_list)
    res_services = pd.DataFrame(res_services_list)

    # Result report
    size = res_hosts.drop('ip_address', axis=1).groupby('host').size()
    result_hosts_tmp = res_hosts.drop(['ip_address', 'disks_primary', 'disks_secondary'], axis=1).groupby('host').sum().assign(number_hosts=size)
    res_services_tmp = res_services.copy()
    list_services_cpu_mem = []
    list_cpu_usage = []
    list_mem_usage = []
    list_cpu_max_usage = []
    list_mem_max_usage = []

    for i in range(result_hosts_tmp.index.size):
        tmp = []
        tmp_cpu = 0
        tmp_mem = 0
        tmp_cpu_max = 0
        tmp_mem_max = 0
        for j in range(res_services_tmp.index.size):
            host_row = result_hosts_tmp.iloc[[i]]
            serv_row = res_services_tmp.iloc[[j]]
            list_node_affinity = res_services_tmp.loc[res_services_tmp.index[j]]['node_affinity_host_type']
            if (serv_row['enabled'][j] and host_row.index[0] in list_node_affinity and (host_row.index[0] != 'cnhost' or host_row.index[0] == 'cnhost' and serv_row['ignore_cnhost_taint'][j] == 1)):
                replicas = 1 + (serv_row['replicas'][j] if serv_row['replicas'][j] != 0 else (
                    int(host_row['number_hosts']) if serv_row['service_name'][j] in CONFIG['exceptions_service'] else 0
                ))
                tmp.append(serv_row['service_name'][j])#[serv_row['service_name'][j], replicas * serv_row['cpu_max'][j], replicas * serv_row['memory_max'][j]])
                tmp_cpu += replicas * serv_row['cpu'][j]
                tmp_mem += replicas * serv_row['memory'][j]
                tmp_cpu_max += replicas * serv_row['cpu_max'][j]
                tmp_mem_max += replicas * serv_row['memory_max'][j]

        list_services_cpu_mem.append(tmp)
        list_cpu_usage.append(tmp_cpu)
        list_mem_usage.append(tmp_mem)
        list_cpu_max_usage.append(tmp_cpu_max)
        list_mem_max_usage.append(tmp_mem_max)

    result_hosts_tmp['services'] = list_services_cpu_mem
    result_hosts_tmp['cpu_services'] = list_cpu_usage
    result_hosts_tmp['cpu_max_services'] = list_cpu_max_usage
    result_hosts_tmp['mem_services'] = list_mem_usage
    result_hosts_tmp['mem_max_services'] = list_mem_max_usage
    """
    for i in range(res_services_tmp.index.size):
        row = res_services_tmp.iloc[[i]]
        list_node_affinity = res_services_tmp.loc[res_services_tmp.index[i]]['node_affinity_host_type']
        for host in list_node_affinity:
            replicas = 1 + (row['replicas'][i] if row['replicas'][i] != 0 else (
                res_hosts_tmp.loc[host]['number_hosts'] if row['service_name'][i] in CONFIG['exceptions_service'] else 0
            ))
            list_cpu_max_usage.append(replicas * row['cpu_max'])
            list_mem_max_usage.append(replicas * row['memory_max'])
            pass
    """
    # Write data to Markdown
    with (open(PATH_OUT, 'w') as file):
        file.write('# hosts \n\n')
        res_hosts_tmp = res_hosts.copy()
        res_hosts_tmp.loc['Total'] = res_hosts_tmp.sum(numeric_only=True)
        res_hosts_overview = res_hosts_tmp.fillna('')
        file.write(res_hosts_overview.to_markdown())
        file.write('\n\n')

        file.write('# group by host \n\n')
        size = res_hosts.drop('ip_address', axis=1).groupby('host').size()
        res_hosts_tmp = res_hosts.drop('ip_address', axis=1).groupby('host').sum().assign(number_hosts=size)
        file.write(res_hosts_tmp.to_markdown())
        file.write('\n\n')

        file.write('# services \n\n')
        res_services_tmp = res_services.copy()
        res_services_tmp.loc['Total'] = res_services_tmp.sum(numeric_only=True)
        res_services_tmp = res_services_tmp.fillna('')
        file.write(res_services_tmp.to_markdown())
        file.write('\n\n')

        res_services_tmp = res_services.drop('container', axis=1).groupby('service_name').sum()
        file.write('# group by services \n\n')
        file.write(res_services_tmp.to_markdown())
        file.write('\n\n')

        file.write('# result \n\n')
        file.write(result_hosts_tmp[['cpu', 'cpu_services', 'cpu_max_services', 'memory', 'mem_services', 'mem_max_services', 'services']].to_markdown())
        file.write('\n\n')

def _runArgParse():

    parser = argparse.ArgumentParser(prog=os.path.basename(sys.argv[0]))
    parser.add_argument('-pin', required=False, help='FILE PATH IN')
    parser.add_argument('-c', required=False, help='CONFIG PATH')
    parser.add_argument('-pout', required=False, help='HOSTS FILE OUTPUT PATH')

    args = parser.parse_args()

    global PATH_IN
    global CONFIG_PATH
    global PATH_OUT

    if args.pin:
        PATH_IN = args.pin
    if args.c:
        CONFIG_PATH = args.c
    if args.pout:
        PATH_OUT = args.pout

def _runConfigParse():
    global CONFIG
    with open(CONFIG_PATH, 'r') as file:
        CONFIG = yaml.safe_load(file)
    if 'cpu_freq' not in CONFIG:
        raise Exception('No found "cpu_freq" in config')
    if 'exceptions_service' not in CONFIG:
        raise Exception('No found "exceptions_service" in config')
    if 'size_gigabyte' not in CONFIG:
        raise Exception('No found "size_gigabyte" in config')

def _runClusterConfigParse():
    global PATH_CLUSTER_CONFIG
    global list_enabled_services
    with open(PATH_CLUSTER_CONFIG, 'r', encoding='utf-8') as file:
        data = yaml.safe_load(file)
    list_enabled_services = data['jobs']['enabled_services']['stateful'] + data['jobs']['enabled_services']['stateless']
    print(list_enabled_services)

if __name__ == "__main__":
    _runArgParse()
    _runConfigParse()
    _runClusterConfigParse()
    main()
